# importing libraries
from PyQt5.QtWidgets import * 
from PyQt5 import QtCore, QtGui, QtNetwork, QtNetwork
from PyQt5.QtGui import * 
from PyQt5.QtCore import QUrl, QTimer
from PyQt5.QtWebKitWidgets import QWebView as QWebView, QWebPage
from PyQt5.QtWebKit import QWebSettings as QWebSettings
import sys


class Window(QMainWindow):
    def __init__(self, parent=None):
        super(Window, self).__init__(parent)

        self.tabWidget = QWebView(self)
        # self.tabWidget.connect(self.tabWidget, QtCore.SIGNAL('loadFinished(bool)'), self.loadFinished)
        self.loadUrl(QUrl('http://banner.tin8.co/utivi/box-banner?codeNumber=bx46d6q3'))
        self.setCentralWidget(self.tabWidget)

    def loadUrl(self, url):
        # self.view = QWebView()
        # self.view.loadFinished.connect(self._on_load_finished)
        # self.view.linkClicked.connect(self.on_linkClicked)
        # self.view.loadStarted.connect(self.on_url_changed)
        # self.tabWidget.setCurrentIndex(self.tabWidget.addTab(self.view, ''))
        # self.tabWidget.showFullScreen()
        req = QtNetwork.QNetworkRequest(QtCore.QUrl(url))
        # req.setRawHeader(b'Accept', b'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9')
        # req.setRawHeader(b'Accept-Encoding', b'gzip, deflate')
        # req.setRawHeader(b'Accept-Language', b'vi,en-GB;q=0.9,en;q=0.8,en-US;q=0.7,ja;q=0.6')
        req.setRawHeader(b'Cache-Control', b'max-age=0')
        req.setRawHeader(b'Connection', b'keep-alive')
        req.setRawHeader(b'Host', b'banner.tin8.co')
        req.setRawHeader(b'Upgrade-Insecure-Requests', b'1')
        req.setRawHeader(b'User-Agent', b'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36')
        req.setMaximumRedirectsAllowed(6)
        settings = QWebSettings.globalSettings()
        
        web_page = QWebPage()
        web_page.setViewportSize(web_page.mainFrame().contentsSize())
        web_page.settings().setAttribute(QWebSettings.LocalContentCanAccessRemoteUrls, True)
        web_page.settings().setAttribute(QWebSettings.LocalContentCanAccessFileUrls, True)
        web_page.settings().setAttribute(QWebSettings.
             JavascriptEnabled,True)
        web_page.settings().setAttribute(QWebSettings.LocalStorageEnabled, True)
        web_page.settings().setAttribute(QWebSettings.AutoLoadImages, True)
        web_page.setLinkDelegationPolicy(QWebPage.DelegateAllLinks)

        
        nam = QtNetwork.QNetworkAccessManager()
        config = nam.configuration()
        config.setConnectTimeout(15000000)
        nam.setConfiguration(config)
        nam.finished.connect(self.finished_loading)
        web_page.setNetworkAccessManager(nam)
        self.tabWidget.setPage(web_page)

        self.tabWidget.loadFinished.connect(self.finished_loading)

        # timeout of 15s
        self.timer = QTimer()
        self.timer.singleShot(15000000, self.finished_loading)
        self.timer.start

        
        self.tabWidget.load(req)
        self.tabWidget.showFullScreen()

    def finished_loading(self, ok):
        print('done')


    # method for widgets
    def UiComponents(self):

        # creating label
        label = QLabel("Button", self)

        # setting geometry to label
        label.setGeometry(100, 100, 120, 40)

        # adding border to label
        label.setStyleSheet("border : 2px solid black")
        self.setWindowTitle('Icon')


        # opening window in maximized size
        self.showFullScreen()
  
  
# create pyqt5 app
App = QApplication(sys.argv)
  
# create the instance of our Window
window = Window()
# window.show()
window.showFullScreen()
# window.setTabShape()

# start the app
sys.exit(App.exec())