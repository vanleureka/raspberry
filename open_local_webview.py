# importing libraries
from PyQt5.QtWidgets import * 
from PyQt5.QtGui import * 
from PyQt5.QtCore import QUrl
# from PyQt5.QtWebEngineWidgets import QWebEngineView as QWebView
import requests
import time
import os
import json
from PyQt5.QtWebKitWidgets import QWebView as QWebView
import sys


def get_serial():
    serial = ''
    f = open('/proc/cpuinfo', 'r')
    for line in f:
        if 'serial' in line.lower():
            serial = line.split(':')[1]
    f.close()
    return serial;
        
def init_request():
    # serial_number = get_serial().strip()
    serial_number = 'bx46d6q3'
    print(serial_number)
    if serial_number == '':
        raise Exception('can not get serial number')
    resp = requests.get('http://tracking.urekamedia.com/api/box-config?codeNumber=' + str(serial_number))
    data = json.loads(resp.text)
    url = data['url'].replace('\n', '').replace('\r', '')
    url += '&rotation=' + data['rotation']
    return (data['url'], data['version'], data['rotation'])
        
class Window(QMainWindow):
    def __init__(self, parent=None):
        super(Window, self).__init__(parent)
        self.webView = QWebView(self)
        self.setCentralWidget(self.webView)
        self.loadUrl()

    def loadUrl(self):
        #self.view = QWebView()        
        old_url, old_version, old_rotation = init_request()
        new_version = old_version
        new_url = old_url
        file_path = os.path.abspath(os.path.join(os.path.dirname(__file__), "get_Banner_Box.html"))
        self.webView.load(QUrl.fromLocalFile(file_path))
        self.webView.showFullScreen()
        time.sleep(30)
        new_url, new_version, old_rotation = init_request()
        if new_version != old_version:
            self.webView.load(QUrl(new_url))
            self.webView.showFullScreen()
  
  
# create pyqt5 app
App = QApplication(sys.argv)

# create the instance of our Window
window = Window()
window.showFullScreen()
  
# start the app
sys.exit(App.exec())