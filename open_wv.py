import sys

from PyQt5.QtWidgets import QMainWindow, QVBoxLayout, QWidget, QApplication
# from PyQt5.QtWebEngineWidgets import *
from PyQt5.QtWebEngineWidgets import QWebEngineView as QWebView

from PyQt5.QtCore import QUrl

app = QApplication(sys.argv)

mainWindow = QMainWindow()
widget = QWidget()


web = QWebView()
web.load(QUrl("http://banner.tin8.co/utivi/box-banner?codeNumber=bx46d6q3"))

verticalLayout = QVBoxLayout()
verticalLayout.addWidget(widget)

widget.setLayout(verticalLayout)
mainWindow.setCentralWidget(widget)
mainWindow.show()

sys.exit(app.exec())