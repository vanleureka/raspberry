# importing libraries
from PyQt5.QtWidgets import * 
from PyQt5.QtGui import * 
from PyQt5.QtCore import QUrl, QTimer
import requests
import time
import json
import constant
from PyQt5.QtWebKitWidgets import QWebView as QWebView
import sys


def get_serial():
    serial = ''
    f = open(constant.CPU_LINK, 'r')
    for line in f:
        if 'serial' in line.lower():
            serial = line.split(':')[1]
    f.close()
    return serial
        
class Window(QMainWindow):
    def __init__(self, parent=None):
        super(Window, self).__init__(parent)
        self.webView = QWebView(self)
        self.setCentralWidget(self.webView)
        
        # init first request
        url, version = self.init_request()
        self._url = url
        self._version = version
        self.webView.load(QUrl(self._url))
        self.webView.showFullScreen()

        # loop over specific time for update webview
        self.timer=QTimer()
        self.timer.timeout.connect(self.update_webview)
        self.timer.start(constant.BREAK_TIME)

    def loadUrl(self):
        print('in loadUrl, ', self._url)
        self.webView.load(QUrl(self._url))
        self.webView.showFullScreen()
        self.showFullScreen()
    
    def finished_loading(self, ok):
        print('finished_loading')

    def update_webview(self):
        url, version = self.init_request()
        print('update_webview each timeout, ', url, version, self._version)
        # update webview if configuration has been updated
        if self._version != version:
            self._url = url
            self.webView.load(QUrl(self._url))

    def init_request(self):
        # serial_number = get_serial().strip()
        # serial_number = '000000001d5cd33b'
        serial_number = 'bx46d6q3'
        print(serial_number)
        if serial_number == '':
            raise Exception('can not get serial number')
        resp = requests.get(constant.API_URL + str(serial_number))
        data = json.loads(resp.text)

        rotation = 'vertical'
        if data['rotation'] is not None and data['rotation'] != "":
            rotation = data['rotation']
        # update rotation each request
        url = data['url'].replace('\n', '').replace('\r', '')
        url += '&rotation=' + rotation
        print('init_request, ', data)

        return url, data['version']
  
  
# create pyqt5 app
App = QApplication(sys.argv)

# create the instance of our Window
window = Window()
window.showFullScreen()

# start the app
sys.exit(App.exec())